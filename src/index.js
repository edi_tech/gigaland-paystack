import React from "react";
import ReactDOM from "react-dom";
import "./assets/animated.css";
import "../node_modules/font-awesome/css/font-awesome.min.css";
import "../node_modules/elegant-icons/style.css";
import "../node_modules/et-line/style.css";
import "../node_modules/bootstrap/dist/css/bootstrap.min.css";
import "../node_modules/bootstrap/dist/js/bootstrap.js";
import "./assets/style.scss";
import App from "./components/app";
import * as serviceWorker from "./serviceWorker";
import { MoralisProvider } from "react-moralis";
import { Moralis } from "moralis";

const APP_ID = "tBervYPJOVX8CXUeuiVWW3R0CGIpgW0d3RMzloJv";
const SERVER_URL = "https://h2najvg6rsdj.usemoralis.com:2053/server";

Moralis.start({ serverUrl: SERVER_URL, appId: APP_ID });

ReactDOM.render(
	<MoralisProvider appId={APP_ID} serverUrl={SERVER_URL}>
		<App />
	</MoralisProvider>,
	document.getElementById("root")
);
// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
