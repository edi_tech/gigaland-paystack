import React, { useEffect, useState } from "react";
import Axios from "axios";
import { message } from "antd";
import Walletx from "../components/wallet";
import Transfer from "../components/Transfer.tsx";
import Footer from "../components/footer";
import { createGlobalStyle } from "styled-components";
import { validateMessages, apiPost, apiGet } from "../helper/index";

const GlobalStyles = createGlobalStyle`
  header#myHeader.navbar.white {
    background: #212428;
  }
  header#myHeader .logo .d-block{
    display: none !important;
  }
  header#myHeader .logo .d-none{
    display: block !important;
  }
  .navbar .mainside a{
    background: #8364e2;
    &:hover{
      box-shadow: 2px 2px 20px 0px #8364e2;
    }
  }
  .item-dropdown{
    .dropdown{
      a{
        &:hover{
          background: #8364e2;
        }
      }
    }
  }
  .btn-main{
    background: #8364e2;
    &:hover{
      box-shadow: 2px 2px 20px 0px #8364e2;
    }
  }
  p.lead{
    color: #a2a2a2;
  }
  .navbar .navbar-item .lines{
    border-bottom: 2px solid #8364e2;
  }
  
  #tsparticles{
    top: 0;
  }
  .text-uppercase.color{
    color: #8364e2;
  }
  .de_count h3 {
    font-size: 36px;
    margin-bottom: 0px;
  }
  .de_count h5{
    font-size: 14px;
    font-weight: 500;
  }
  h2 {
    font-size: 30px;
  }
  .box-url{
    
    h4{
      font-size: 16px;
    }
  }
  .de_countdown{
    border: solid 2px #8364e2;
  }
  .author_list_pp, .author_list_pp i, 
  .nft_coll_pp i, .feature-box.style-3 i, 
  footer.footer-light #form_subscribe #btn-subscribe i, 
  #scroll-to-top div{
    background: #8364e2;
  }
  footer.footer-light .subfooter .social-icons span i{
    background: #403f83;
  }
  .author_list_pp:hover img{
    box-shadow: 0px 0px 0px 2px #8364e2;
  }
  .nft__item_action span{
    color: #8364e2;
  }
  .feature-box.style-3 i.wm{
    color: rgba(131,100,226, .1);
  }
  @media only screen and (max-width: 1199px) {
    .navbar{
      
    }
    .navbar .menu-line, .navbar .menu-line1, .navbar .menu-line2{
      background: #fff;
    }
    .item-dropdown .dropdown a{
      color: #fff !important;
    }
  }
`;
const KEY = process.env.REACT_APP_KEY;

const Wallet = () => {
  const [subaccount, setSubaccount] = useState([]);
  const [balance, setBalance] = useState(30000);
  const [amount, setAmount] = useState();
  const [transferrecipient, setTransferrecipient] = useState("");

  useEffect(() => {
    const fetchSubaccount = () => {
      // const schedule = { settlement_schedule: "manual" };
      // const config = {
      //   headers: {
      //     Authorization: `Bearer ${KEY}`,
      //   },
      // };
      // Axios.get("https://api.paystack.co/subaccount/ACCT_8l3u5tr72k8zcfi", config)
      //   .then(console.log)
      //   .catch(console.log);
    };
    apiGet(`subaccount/ACCT_8l3u5tr72k8zcfi`)
      .then(
        (response) => (
          setSubaccount(response.data?.data), console.log(response.data)
        )
      )
      .catch((e) => {
        message.error(e.message);
      });

      apiGet(`transaction/totals`)
      .then(
        (response) => (
          // setBalance(response.data?.data), 
          console.log(response.data)
        )
      )
      .catch((e) => {
        message.error(e.message);
      });
      
      
  }, []);

  

    useEffect(() => {
  
    }, [transferrecipient]);
  

  const withdraw = () => {
    const formValues = {
      "type": "nuban",
      "currency": "NGN",
      "name": subaccount.business_name,
      "account_number": subaccount.account_number,
      "bank_code": "057",
      "metadata": {
        'amount': amount
      }
    }
    apiPost(`transferrecipient`, formValues).then((response) => {
      
      setTransferrecipient(response.data?.data.recipient_code.toString())
      
      console.log(response.data?.data.recipient_code.toString())
      transfer()
    })
      .catch((e) => {
        message.error(e.message);
        console.log(e.message)
      })
  } 

  const transfer = () => {
    console.log(transferrecipient)
    const transferValues = {
      "source": "balance",
      "reason": "Test",
      "amount": amount * 100,
      "recipient": "RCP_ut2pccnpqxvurda",
      
    }
    apiPost(`transfer`, transferValues).then((response) => {
      setBalance(balance - amount)
      alert("Success")
      console.log(response)
    })
      .catch((e) => {
        message.error(e.message)
        alert("Fialed")
        console.log(e.message)
      })
  }

  return (
    <div>
      <GlobalStyles />

      <section className="jumbotron breadcumb no-bg">
        <div className="mainbreadcumb">
          <div className="container">
            <div className="row m-10-hor">
              <div className="col-12">
                <h1 className="text-center">Wallet</h1>

                <div className="de_tab">
                  <ul className="de_nav">
                    <li className="">
                      <span onClick="">NGN {balance}</span>
                    </li>

                    {/* <li id='Mainbtn' className="active"><span onClick={handleBtnClick}>Bids</span></li>
                                    <li id='Mainbtn1' className=''><span onClick={handleBtnClick1}>History</span></li> */}
                  </ul>
                </div>
                <br />
                <div className="de_tab">
                  <ul className="de_nav">
                    <li className="">
                      
                        <input
                          type="number"
                          value={amount}
                          onChange={(e) => setAmount(e.target.value)}
                        />
                   
                    </li>
                    <li className="" style={{ backgroundColor: "#8364e2" }}>
                      <span style={{ color: "white" }} onClick={withdraw}>
                        Withdraw
                      </span>
                    </li>
                    {/* <li id='Mainbtn' className="active"><span onClick={handleBtnClick}>Bids</span></li>
                                    <li id='Mainbtn1' className=''><span onClick={handleBtnClick1}>History</span></li> */}
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

      <section className="container">
        <Walletx />
      </section>

      <Footer />
    </div>
  );
};
export default Wallet;
