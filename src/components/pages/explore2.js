import React from 'react';
import ColumnNewThreeCol from '../components/ColumnNewThreeCol';
import Footer from '../components/footer';
import { createGlobalStyle } from 'styled-components';

const GlobalStyles = createGlobalStyle`
  .navbar {
    border-bottom: solid 1px rgba(255, 255, 255, .1) !important;
  }
  header#myHeader .logo .d-block{
    display: none !important;
  }
  header#myHeader .logo .d-none{
    display: block !important;
  }
  .navbar .mainside a{
    background: #8364e2;
    &:hover{
      box-shadow: 2px 2px 20px 0px #8364e2;
    }
  }
  .item-dropdown{
    .dropdown{
      a{
        &:hover{
          background: #8364e2;
        }
      }
    }
  }
  .btn-main{
    background: #8364e2;
    &:hover{
      box-shadow: 2px 2px 20px 0px #8364e2;
    }
  }
  p.lead{
    color: #a2a2a2;
  }
  .navbar .navbar-item .lines{
    border-bottom: 2px solid #8364e2;
  }
  .jumbotron.no-bg{
    height: 100vh;
    overflow: hidden;
    background-repeat: repeat;
    background-size: cover;
    background-position: bottom;
    background-repeat: no-repeat;
  }
  #tsparticles{
    top: 0;
  }
  .text-uppercase.color{
    color: #8364e2;
  }
  .de_count h3 {
    font-size: 36px;
    margin-bottom: 0px;
  }
  .de_count h5{
    font-size: 14px;
    font-weight: 500;
  }
  h2 {
    font-size: 30px;
  }
  .box-url{
    text-align: center;
    h4{
      font-size: 16px;
    }
  }
  .de_countdown{
    border: solid 2px #8364e2;
  }
  .author_list_pp, .author_list_pp i, 
  .nft_coll_pp i, .feature-box.style-3 i, 
  footer.footer-light #form_subscribe #btn-subscribe i, 
  #scroll-to-top div{
    background: #8364e2;
  }
  footer.footer-light .subfooter .social-icons span i{
    background: #403f83;
  }
  .author_list_pp:hover img{
    box-shadow: 0px 0px 0px 2px #8364e2;
  }
  .nft__item_action span{
    color: #8364e2;
  }
  .feature-box.style-3 i.wm{
    color: rgba(131,100,226, .1);
  }
  @media only screen and (max-width: 1199px) {
    .navbar{
      
    }
    .navbar .menu-line, .navbar .menu-line1, .navbar .menu-line2{
      background: #fff;
    }
    .item-dropdown .dropdown a{
      color: #fff !important;
    }
  }
`;


const explore= () => (
<div>
<GlobalStyles/>
  <section className='container'>
        <div className='row'>
        <div className="spacer-double"></div>

          <div className='col-md-3'>

          <div className="item_filter_group">
              <h4>Select Categories</h4>
              <div className="de_form">
                  <div className="de_checkbox">
                      <input id="check_cat_1" name="check_cat_1" type="checkbox" value="check_cat_1"/>
                      <label htmlFor="check_cat_1">Art</label>
                  </div>

                  <div className="de_checkbox">
                      <input id="check_cat_2" name="check_cat_2" type="checkbox" value="check_cat_2"/>
                      <label htmlFor="check_cat_2">Music</label>
                  </div>

                  <div className="de_checkbox">
                      <input id="check_cat_3" name="check_cat_3" type="checkbox" value="check_cat_3"/>
                      <label htmlFor="check_cat_3">GIFS</label>
                  </div>

                  <div className="de_checkbox">
                      <input id="check_cat_4" name="check_cat_4" type="checkbox" value="check_cat_4"/>
                      <label htmlFor="check_cat_4">Virtual World</label>
                  </div>

                  <div className="de_checkbox">
                      <input id="check_cat_5" name="check_cat_5" type="checkbox" value="check_cat_5"/>
                      <label htmlFor="check_cat_5">Trading Cards</label>
                  </div>

                  <div className="de_checkbox">
                      <input id="check_cat_6" name="check_cat_6" type="checkbox" value="check_cat_6"/>
                      <label htmlFor="check_cat_6">Collectibles</label>
                  </div>

                  <div className="de_checkbox">
                      <input id="check_cat_7" name="check_cat_7" type="checkbox" value="check_cat_7"/>
                      <label htmlFor="check_cat_7">Sports</label>
                  </div>

                  <div className="de_checkbox">
                      <input id="check_cat_8" name="check_cat_8" type="checkbox" value="check_cat_8"/>
                      <label htmlFor="check_cat_8">Utility</label>
                  </div>

              </div>
          </div>

          <div className="item_filter_group">
              <h4>Status</h4>
              <div className="de_form">
                  <div className="de_checkbox">
                      <input id="buy" name="buy" type="checkbox" value="buy"/>
                      <label htmlFor="buy">Buy Now</label>
                  </div>

                  <div className="de_checkbox">
                      <input id="onauction" name="onauction" type="checkbox" value="onauction"/>
                      <label htmlFor="onauction">Free Items</label>
                  </div>

                  <div className="de_checkbox">
                      <input id="offers" name="offers" type="checkbox" value="offers"/>
                      <label htmlFor="offers">Sale</label>
                  </div>

              </div>
          </div>

          <div className="item_filter_group">
              <h4>Items Type</h4>
              <div className="de_form">
                  <div className="de_checkbox">
                      <input id="sitems" name="sitems" type="checkbox" value="sitems"/>
                      <label htmlFor="sitems">Single Items</label>
                  </div>

                  <div className="de_checkbox">
                      <input id="bundles" name="bundles" type="checkbox" value="bundles"/>
                      <label htmlFor="bundles">Bundles</label>
                  </div>

              </div>
          </div>

          <div className="item_filter_group">
              <h4>Collections</h4>
              <div className="de_form">
                  <div className="de_checkbox">
                      <input id="abstract" name="abstract" type="checkbox" value="abstract"/>
                      <label htmlFor="abstract">Abstraction</label>
                  </div>

                  <div className="de_checkbox">
                      <input id="paterns" name="paterns" type="checkbox" value="paterns"/>
                      <label htmlFor="paterns">Patternlicious</label>
                  </div>

                  <div className="de_checkbox">
                      <input id="skecth" name="skecth" type="checkbox" value="skecth"/>
                      <label htmlFor="skecth">Skecthify</label>
                  </div>

                  <div className="de_checkbox">
                      <input id="cartoon" name="cartoon" type="checkbox" value="cartoon"/>
                      <label htmlFor="cartoon">Cartoonism</label>
                  </div>

                  <div className="de_checkbox">
                      <input id="virtualand" name="virtualand" type="checkbox" value="virtualand"/>
                      <label htmlFor="virtualand">Virtuland</label>
                  </div>

                  <div className="de_checkbox">
                      <input id="pappercut" name="pappercut" type="checkbox" value="pappercut"/>
                      <label htmlFor="pappercut">Papercut</label>
                  </div>

              </div>
          </div>

          </div>

          <div className="col-md-9">
            <ColumnNewThreeCol/>
          </div>
        </div>
      </section>


  <Footer />
</div>

);
export default explore;