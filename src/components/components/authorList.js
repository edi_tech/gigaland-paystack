import React from 'react';

const authorlist= () => (
  <div>
    <ol className="author_list">
      <li>                                    
          <div className="author_list_pp">
              <span onClick={()=> window.open("", "_self")}>
                  <img className="lazy" src="./img/author/author-1.jpg" alt=""/>
                  <i className="fa fa-check"></i>
              </span>
          </div>                                    
          <div className="author_list_info">
              <span onClick={()=> window.open("", "_self")}>Monica Lucas</span>
              <span className="bot">NGN 700,000</span>
          </div>
      </li>
      <li>
          <div className="author_list_pp">
              <span onClick={()=> window.open("", "_self")}>                                    
                  <img className="lazy" src="./img/author/author-2.jpg" alt=""/>
                  <i className="fa fa-check"></i>
              </span>
          </div>
          <div className="author_list_info">
              <span onClick={()=> window.open("", "_self")}>Mamie Barnett</span>
              <span className="bot">NGN 650,000</span>
          </div>
      </li>
      <li>
          <div className="author_list_pp">
              <span onClick={()=> window.open("", "_self")}>                                    
                  <img className="lazy" src="./img/author/author-3.jpg" alt=""/>
                  <i className="fa fa-check"></i>
              </span>
          </div>
          <div className="author_list_info">
              <span onClick={()=> window.open("", "_self")}>Nicholas Daniels</span>
              <span className="bot">NGN 600,000</span>
          </div>
      </li>
      <li>
          <div className="author_list_pp">
              <span onClick={()=> window.open("", "_self")}>                                    
                  <img className="lazy" src="./img/author/author-4.jpg" alt=""/>
                  <i className="fa fa-check"></i>
              </span>
          </div>
          <div className="author_list_info">
              <span onClick={()=> window.open("", "_self")}>Lori Hart</span>
              <span className="bot">NGN 550,000</span>
          </div>
      </li>
      <li>
          <div className="author_list_pp">
              <span onClick={()=> window.open("", "_self")}>                                    
                  <img className="lazy" src="./img/author/author-5.jpg" alt=""/>
                  <i className="fa fa-check"></i>
              </span>
          </div>
          <div className="author_list_info">
              <span onClick={()=> window.open("", "_self")}>Jimmy Wright</span>
              <span className="bot">NGN 500,000</span>
          </div>
      </li>
      <li>
          <div className="author_list_pp">
              <span onClick={()=> window.open("", "_self")}>                                    
                  <img className="lazy" src="./img/author/author-6.jpg" alt=""/>
                  <i className="fa fa-check"></i>
              </span>
          </div>
          <div className="author_list_info">
              <span onClick={()=> window.open("", "_self")}>Karla Sharp</span>
              <span className="bot">NGN 450,000</span>
          </div>
      </li>
      <li>
          <div className="author_list_pp">
              <span onClick={()=> window.open("", "_self")}>                                    
                  <img className="lazy" src="./img/author/author-7.jpg" alt=""/>
                  <i className="fa fa-check"></i>
              </span>
          </div>
          <div className="author_list_info">
              <span onClick={()=> window.open("", "_self")}>Gayle Hicks</span>
              <span className="bot">NGN 400,000</span>
          </div>
      </li>
      <li>
          <div className="author_list_pp">
              <span onClick={()=> window.open("", "_self")}>                                    
                  <img className="lazy" src="./img/author/author-8.jpg" alt=""/>
                  <i className="fa fa-check"></i>
              </span>
          </div>
          <div className="author_list_info">
              <span onClick={()=> window.open("", "_self")}>Claude Banks</span>
              <span className="bot">NGN 350,000</span>
          </div>
      </li>
      <li>
          <div className="author_list_pp">
              <span onClick={()=> window.open("", "_self")}>                                    
                  <img className="lazy" src="./img/author/author-9.jpg" alt=""/>
                  <i className="fa fa-check"></i>
              </span>
          </div>
          <div className="author_list_info">
              <span onClick={()=> window.open("", "_self")}>Franklin Greer</span>
              <span className='bot'>NGN 300,000</span>
          </div>
      </li>
      <li>
          <div className="author_list_pp">
              <span onClick={()=> window.open("", "_self")}>                                    
                  <img className="lazy" src="./img/author/author-10.jpg" alt=""/>
                  <i className="fa fa-check"></i>
              </span>
          </div>
          <div className="author_list_info">
              <span onClick={()=> window.open("", "_self")}>Stacy Long</span>
              <span className='bot'>NGN 250,000</span>
          </div>
      </li>
      <li>
          <div className="author_list_pp">
              <span onClick={()=> window.open("", "_self")}>                                    
                  <img className="lazy" src="./img/author/author-11.jpg" alt=""/>
                  <i className="fa fa-check"></i>
              </span>
          </div>
          <div className="author_list_info">
              <span onClick={()=> window.open("", "_self")}>Ida Chapman</span>
              <span className='bot'>NGN 200,000</span>
          </div>
      </li>
      <li>
          <div className="author_list_pp">
              <span onClick={()=> window.open("", "_self")}>                                    
                  <img className="lazy" src="./img/author/author-12.jpg" alt=""/>
                  <i className="fa fa-check"></i>
              </span>
          </div>
          <div className="author_list_info">
              <span onClick={()=> window.open("", "_self")}>Fred Ryan</span>
              <span className='bot'>NGN 150,000</span>
          </div>
      </li>
    </ol>
  </div>
);
export default authorlist;